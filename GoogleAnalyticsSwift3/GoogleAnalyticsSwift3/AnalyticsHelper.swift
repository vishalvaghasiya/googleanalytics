//
//  AnalyticsHelper.swift
//  LostAndFound
//
//  Created by KMSOFT on 23/06/17.
//  Copyright © 2017 ISLServices. All rights reserved.
//

import Foundation
import Google
import GGLAnalytics
import UIKit

class AnalyticsHelper {
    
    func analyticLogScreen (screen: String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: screen)
        
        let build = (GAIDictionaryBuilder.createScreenView().build() as NSDictionary) as! [AnyHashable: Any]
        tracker?.send(build)
    }
    
    func analyticLogAction(category: String, action: String) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIEventAction, value: action)
        
        let build = (GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: nil, value: nil).build() as NSDictionary) as! [AnyHashable: Any]
        tracker?.send(build)
        
        
    }
}



//extension UIViewController {
//    
//    func setScreenName(name: String) {
//        self.title = name
//        self.sendScreenView()
//    }
//    
//    func sendScreenView() {
//        weak var tracker = GAI.sharedInstance().defaultTracker
//        tracker!.set(kGAIScreenName, value: self.title)
//        tracker!.send(GAIDictionaryBuilder.createScreenView()?.build() as [NSObject : AnyObject]!)
//    }
//}
